@extends('layouts.app')

@section('content')
<div class="container">
    @include('messages')
    <h1>Artists <a class="btn btn-primary float-right" href="{{config('app.url')}}/artists/create">Add New Artist</a></h1>
    
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Artist</th>
                <th scope="col">Twitter Handle</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($artists as $artist)
            <tr>
                <th scope="row">{{$artist->id}}</th>
                <td>{{$artist->name}}</td>
                <td>
                    <a href="https://twitter.com/{{$artist->twitter}}" target="_blank">{{"@".$artist->twitter}}</a>
                </td>
                <td>
                    <form action="{{ route('artists.destroy',$artist->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a class="btn btn-outline-dark" href="{{config('app.url')}}/artists/{{$artist->id}}/edit">Edit</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection