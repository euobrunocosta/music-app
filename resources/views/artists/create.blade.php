@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add new Artist</h1>
        <form method="post" action="{{ route('artists.store') }}">
            <div class="form-group">
                @csrf            
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name">
            </div>
            <div class="form-group">
                <label for="twitter">Twitter Handle</label>
                <input type="text" class="form-control" name="twitter">
                <small class="text-secondary">Without the '@' sign</small>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ config('app.url') }}/artists" class="btn btn-outline-dark">Go Back</a>
        </form>
    </div>
@endsection