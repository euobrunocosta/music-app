@extends('layouts.app')

@section('content')
<div class="container">
    @include('messages')
    <h1>Albums<a class="btn btn-primary float-right" href="{{config('app.url')}}/albums/create">Add New Album</a></h1>
    
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Album</th>
                <th scope="col">Artist</th>
                <th scope="col">Year</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($albums as $album)
            <tr>
                <th scope="row">{{$album->id}}</th>
                <td>{{$album->title}}</td>
                <td>{{$album->artist->name}}</td>
                <td>{{$album->year}}</td>
                <td>
                    <form action="{{ route('albums.destroy',$album->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a class="btn btn-outline-dark" href="{{config('app.url')}}/albums/{{$album->id}}/edit">Edit</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection