@extends('layouts.app')

@section('content')
    <div class="container">
        @include('messages')
        <h1>Edit Album</h1>
        <form method="post" action="{{ route('albums.update', $dados['album']->id) }}">
            <div class="form-group">
                @method('PUT')
                @csrf
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" value="{{$dados['album']->title}}">
            </div>
            <div class="form-group">
                <label for="artist_id">Artist</label>
                <select class="form-control" name="artist_id">
                    <option value="">Select an artist</option>
                    @foreach ($dados['artists'] as $artist)
                    <option
                        value="{{$artist->id}}"
                        {{($dados['album']->artist_id == $artist->id ? 'selected' : '')}}>
                        {{$artist->name}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input type="text" class="form-control" name="year" value="{{$dados['album']->year}}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ config('app.url') }}/albums" class="btn btn-outline-dark">Go Back</a>
        </form>
    </div>
@endsection