@extends('layouts.app')

@section('content')
    <div class="container">
        @include('messages')
        <h1>Add new Album</h1>
        <form method="post" action="{{ route('albums.store') }}">
            <div class="form-group">
                @csrf            
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title">
            </div>
            <div class="form-group">
                <label for="artist_id">Artist</label>
                <select class="form-control" name="artist_id">
                    <option value="">Select an artist</option>
                    @foreach ($artists as $artist)
                    <option value="{{$artist->id}}">{{$artist->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input type="text" class="form-control" name="year">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ config('app.url') }}/albums" class="btn btn-outline-dark">Go Back</a>
        </form>
    </div>
@endsection